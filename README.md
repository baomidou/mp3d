# 普拉斯 MybatisPlus

#### 介绍
该仓库为 MybatisPlus 吉祥物 “ 普拉斯 ” 2D 平面 + 3D 建模源文件，免费开源授权广大爱好者使用（个人免费，商用请联系作者）

[MybatisPlus](https://baomidou.com/)

[LOGO 源文仓库](https://gitee.com/baomidou/logo)

压缩包 `普拉斯3D.zip` 为玛雅3D源文件，可以直接使用3D打印机打印普拉斯手办。

# 玛雅 3D 效果图

- 打印手办玩偶

![普拉斯3D建模图](mp3d.png)

# 三视平面效果图

- 印刷文化衫

![普拉斯平面三视图](mp2d.png)

